﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;


namespace CoolParkng.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringVariant;
            ParkingService parkingService = new ParkingService(new TimerService(Settings.PaymentPeriod), new TimerService(Settings.LogPeriod), new LogService($@"..\..\..\Transactions.log"));
            parkingService.moneyIsDrawn += NotifyModeyIsDrawn;
            
            
            Console.WriteLine("Hi! This is a parking simulator");
            do
            {
                Console.WriteLine("Choose your variant:");
                Console.WriteLine("1. Display the current balance of the Parking.\n2. Display the amount of money earned for the current period(before logging).\n" +
                    "3. Display the number of free/occupied parking spaces.\n4. Display all Parking Transactions for the current period(before being logged).\n" +
                    "5. Print the transaction history(by reading data from the Transactions.log file).\n6. Display the list of vehicles are located on the Parking.\n" +
                    "7. Add vehicle to trhe Parking\n8. Pick up a vehicle from the Parking.\n9. Top up the balance of a vehicle.\n0. Exit.\n");

                Validate(out stringVariant, 0, 9);
                switch (Byte.Parse(stringVariant))
                {
                    case 1:
                        {
                            Console.WriteLine($"{parkingService.GetBalance()} - Parking balance");
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine($"{parkingService.GetLastEarnedMoney()} - Earned money for last period");
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine($"{parkingService.GetFreePlaces()-1} - Parking free places");
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("All Parking Transactions for the current period:");
                            foreach (TransactionInfo transaction in parkingService.GetLastParkingTransactions())
                            {
                                Console.WriteLine(transaction.ToString());
                            }
                            break;
                        }
                    case 5:
                        {

                            Console.WriteLine($"All transaction history: \n{parkingService.ReadFromLog()}");
                            break;
                        }
                    case 6:
                        {
                            Console.WriteLine("All vehicles in the Parking:");
                            foreach (Vehicle vehicle in parkingService.GetVehicles())
                            {
                                Console.WriteLine(vehicle.ToString());
                            }
                            break;
                        }
                    case 7:
                        {
                            try
                            {
                                string id = default;
                                VehicleType vehicleType = default;
                                string balance = default;
                                Console.WriteLine("Write a number. Write own number (write 1) or generate (write 2)");
                                Validate(out stringVariant, 1, 2);
                                switch (Byte.Parse(stringVariant))
                                {
                                    case 1:
                                        Console.Write("Input the number of type ХХ-YYYY-XX (like DV-2345-KJ):");
                                        id = Console.ReadLine();
                                        break;
                                    case 2:
                                        id = Vehicle.GenerateRandomRegistrationPlateNumber();
                                        Console.WriteLine($"Number is {id}");
                                        break;
                                }
                                Console.WriteLine("Chose type of vehicle (1 - Passenger Car, 2 - Truck, 3 - Bus, 4 - Motorcycle.");
                                Validate(out stringVariant, 1, 4);
                                switch (Byte.Parse(stringVariant))
                                {
                                    case 1:
                                        vehicleType = VehicleType.PassengerCar;
                                        Console.WriteLine("This vehicle is Passenger Car");
                                        break;
                                    case 2:
                                        vehicleType = VehicleType.Truck;
                                        Console.WriteLine("This vehicle is Truck");
                                        break;
                                    case 3:
                                        vehicleType = VehicleType.Bus;
                                        Console.WriteLine("This vehicle is Bus");
                                        break;
                                    case 4:
                                        vehicleType = VehicleType.Motorcycle;
                                        Console.WriteLine("This vehicle is Motorcycle");
                                        break;
                                }
                                Console.Write("Input vehicle balance sum: ");
                                Validate(out balance, 1, Int32.MaxValue);
                                parkingService.AddVehicle(new Vehicle(id, vehicleType, Decimal.Parse(balance)));
                            }
                            catch (ArgumentException ex)
                            {
                                WriteWrongMessage(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                WriteWrongMessage(ex.Message);
                            }
                            break;
                        }
                    case 8:
                        {
                            string numberToRemove;
                            try
                            {
                                Console.WriteLine("Write a number.");
                                numberToRemove = Console.ReadLine();
                                parkingService.RemoveVehicle(numberToRemove);
                            }
                            catch (ArgumentException ex)
                            {
                                WriteWrongMessage(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                WriteWrongMessage(ex.Message);
                            }
                            break;
                        }
                    case 9:
                        {
                            try
                            {
                                string numberToAdd;
                                string balanceToAdd;
                                Console.WriteLine("Write a number.");
                                numberToAdd = Console.ReadLine();
                                Console.Write("Input vehicle balance sum: ");
                                Validate(out balanceToAdd, 1, Int32.MaxValue);
                                parkingService.TopUpVehicle(numberToAdd, Decimal.Parse(balanceToAdd));
                            }
                            catch (ArgumentException ex)
                            {
                                WriteWrongMessage(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                WriteWrongMessage(ex.Message);
                            }
                            break;
                        }
                }
            }
            while (Byte.Parse(stringVariant) != 0);
            parkingService.Dispose();

            static void Validate(out string stringVariant, int from, int to)
            {
                do
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Green;
                    stringVariant = Console.ReadLine();
                    Console.ForegroundColor = ConsoleColor.White;
                    if (!IsRightInDiapason(stringVariant, from, to))
                    {
                        WriteWrongMessage("Error! Incorrect input");
                    }
                }
                while (!IsRightInDiapason(stringVariant, from, to));
            }


            static bool IsRightInDiapason(string stringVariant, int from, int to)
            {
                int variant;

                if (Int32.TryParse(stringVariant, out variant))
                    if (variant >= from && variant <= to)
                        return true;
                    else
                        return false;
                else
                    return false;
            }

            static void NotifyModeyIsDrawn()
            {
                Console.Beep();
            }

            static void WriteWrongMessage(string message)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
