﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance { get; }
        public static int Capicity { get; }
        public static int PaymentPeriod { get; }
        public static int LogPeriod { get; }
        public static Dictionary<VehicleType, decimal> Tariffs { get; }
        public static decimal PenaltyRate { get; }

        static Settings()
        {
            Balance = 0;
            Capicity = 10;
            PaymentPeriod = 5000;
            LogPeriod = 60000;
            Tariffs = new Dictionary<VehicleType, decimal>
            {
                {VehicleType.PassengerCar, 2M},
                {VehicleType.Truck, 5M},
                {VehicleType.Bus, 3.5M},
                {VehicleType.Motorcycle, 1M}
            };
            PenaltyRate = 2.5M;
        }
    }
}