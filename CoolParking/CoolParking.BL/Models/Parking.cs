﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private static Parking _instance;
        private static List<Vehicle> _vehiclesList = new List<Vehicle>();

        private Parking()
        {

        }

        public static Parking Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Parking();
                }
                return _instance;
            }
        }
        public decimal Balance { get; set; }

        public void AddVehicle(Vehicle vehicle)
        {
            _vehiclesList?.Add(vehicle);
        }

        public void RemoveVehicle(Vehicle vehicle)
        {
            _vehiclesList?.Remove(vehicle);
        }

        public List<Vehicle> GetVehicles()
        {
            return _vehiclesList;
        }

        public int GetOccupiedPlaces()
        {
            return _vehiclesList.Count;
        }

        public static bool IsNumberExist(string vehicleId)
        {
            Vehicle searchedVehicle = FindById(vehicleId);
            if (searchedVehicle == null)
                return false;
            else
                return true;
        }

        public static Vehicle FindById(string vehicleId)
        {
            Vehicle searchedVehicle = _vehiclesList.Where(s => s.Id == vehicleId).FirstOrDefault();
            return searchedVehicle;
        }
        internal void Clear()
        {
            _vehiclesList.Clear();
            Balance = 0;
        }
    }
}