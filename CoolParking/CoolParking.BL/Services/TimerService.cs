﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;

        public event ElapsedEventHandler Elapsed;

        public TimerService(int interval)
        {
            _timer = new Timer();
            Interval = interval;
            _timer.Elapsed += TickTimer;
        }
        private void TickTimer(object sender, ElapsedEventArgs e)
        {
            Elapsed.Invoke(sender, e);
        }

        public double Interval 
        { 
            get => _timer.Interval;
            set
            {
                if (value > 0)
                    _timer.Interval = value;
                else
                    throw new Exception("Negative interval");
            }
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}